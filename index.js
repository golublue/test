const 
    koa = require('koa'),
    koaPug = require('koa-pug'),
    router = require('./src/router'),
    app = new koa(),
    pug = new koaPug({
        viewPath: './src/pug',
        noCache: true,
        app
    })

const init = (app) => {
    app
        .use(require('koa-static')(`${__dirname}/public`))
        .use(router.routes()).use(router.allowedMethods())

    // app.use((ctx) => {
    //     ctx.render('pages/error', {title: "Ошибка"})
    // })

    const port = process.env.PORT || 3000;
    app.listen(port, () => {
        console.log("App is running on port " + port);
    });
}

if(module.parent) {
    module.exports = init
} else {
    init(app)
}