import Vue from 'Vue'

import ProfileTab from '../components/profile-tab.vue'
import FriendsTab from '../components/friends-tab.vue'
import AddInterestForm from '../components/add-interest-form.vue'

Vue.component('profile-tab', ProfileTab)
Vue.component('friends-tab', FriendsTab)
Vue.component('add-interest-form', AddInterestForm)

new Vue({ el: '#vue'})