const
    koaRouter = require('koa-router'),
    router = new koaRouter(),

    index = require('./controllers/index')

router
    .get('/', index)

module.exports = router