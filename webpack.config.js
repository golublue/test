const path = require('path')

module.exports = {
    mode: 'production',
    entry: [
        './src/js/index.js',
        './src/scss/app.scss', //not-component-specific scss goes there
    ],
    output: {
        path: __dirname + "/public",
        filename: './bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [{
                        loader: 'file-loader',
                        options: {
                            name: '[name].css',
                        }
                    }, {
                        loader: 'extract-loader'
                    }, {
                        loader: 'css-loader'
                    }, {
                        loader: 'postcss-loader'
                    }, {
                        loader: 'sass-loader'
                    }
                ]
            },
            {
                test: /\.css$/,
                use: [{
                    loader: 'style-loader'
                }, {
                    loader: 'css-loader'
                }]
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    postLoaders: {
                        html: 'babel-loader'
                    },
                }
            },
            { 
                test: /\.(png|woff|woff2|eot|ttf|svg|otf)$/, 
                loader: 'file-loader' 
            }
        ]
    },
    resolve: {
        modules: ['node_modules', path.resolve(__dirname, 'src')],
        alias: {
            BootstrapVue: path.resolve(__dirname, './node_modules/bootstrap-vue/dist/bootstrap-vue.css'),
            Vue: 'vue/dist/vue.js'
        }
    }
}